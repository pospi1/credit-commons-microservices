<?php
$node_name = basename(__DIR__);
// This header is required in the API
header('Node-name: '.basename(__DIR__));
if (is_dir('../ccclient')) {
  $client_url = "http://".str_replace($node_name, 'ccclient', $_SERVER['SERVER_NAME']);
}
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">
  <head>
    <title>Credit Commons client</title>
  </head>
  <body>
    <p>This is a Credit Commons node called '<?php print $node_name?>'.</p>
    <p>It consists of microservices each on its own subdomain of <?php print $_SERVER['SERVER_NAME']; ?> which can only be accessed through its REST API<?php if (isset($client_url)) print ", however this server may have a <a href=\"$client_url\">developer client</a>";?>.</p>
    <p>This url could be used as a public front page for the node, showing stats etc.
    <p>For more information please see <a href="http://creditcommons.net">creditcommons.net</a>.</p>
  </body>
</html>