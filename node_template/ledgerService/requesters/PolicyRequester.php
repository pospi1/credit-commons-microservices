<?php

/**
 * Handle requests & responses from the ledger to the policy services.
 */
class PolicyRequester extends BaseRequester {

  const SUBDOMAIN = 'policy';

  /**
   * Cache of fetched policies.
   * @var array
   * @todo probably necessary to find a way to force cache reset.
   */
  protected $fetched = [];

  /**
   * Filter on the account names
   *
   * @param array $params
   *   possible keys are chars, status, local
   * @param type $status
   *   If bool, filter by that value
   * @param string $view_mode
   *   one of full, override, nameonly
   * @return array
   *   the matching account names.
   */
  function filter(array $params = [], $view_mode = 'full') : array {
    foreach ($params as $key => $val) {
      if (strlen($val)) {
        $this->addQueryParam($key, (string)$val);
      }
    }
    list($code, $accounts) = $this
      ->addQueryParam('view_mode', $view_mode)
      ->accept(200)
      ->request('filter');
    return (array)$accounts;
  }

  /**
   * Override account defaults.
   *
   * @param string $name
   * @param array $vals
   * @return stdClass|NULL
   *
   * @todo update the swagger - this replaces the block method
   */
 function override(string $name, array $vals) {
    $this->setBody($vals);
    list($code, $policy) = $this->setMethod('patch')
      ->accept(200, 400, 404)
      ->request('override/'.$name);
    switch($code) {
      case 200:
        return $policy;
      case 400:
        cc_log($policy, 'invalid fields');
        return;
      case 404:
        cc_log($policy, 'invalid name');
    }
  }

  /**
   * Get the policy for one account.
   *
   * Use this if you know the account exists.
   *
   * @param string $name
   * @param string $view_mode
   * @return stdClass|NULL
   *   The policy object having been through json.
   */
  function fetch(string $name, string $view_mode = "full") {
    list($code, $result) = $this
      ->accept(200, 403, 404)
      ->addQueryParam('view_mode', $view_mode)
      ->request('fetch/'.$name);
      // the name might have been deleted because of GDPR
    if ($code == 200) return $result;
  }


  /**
   * Create a new account with settings or defaults.
   * @param string $name
   * @return stdClass|null
   *   An object representing the group's policy towards the new account
   */
  function join($name) {
    cc_message("Creating a new account with name: $name");
    list($code, $accountPolicy) = $this->setMethod('post')
      ->accept(201, 400)
      ->addField('name', $name)
      ->request('join');

    switch ($code) {
      case 201:
        return $accountPolicy;
      case 400:
        trigger_error("An account named '$name' already exists.", E_USER_ERROR);
        break;
    }
  }

}
