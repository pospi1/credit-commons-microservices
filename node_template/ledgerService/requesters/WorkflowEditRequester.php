<?php

/**
 * Calls to the Workflow Service
 * @deprecated
 */
class WorkflowRequester extends BaseRequester {

  const SUBDOMAIN = 'workflow';

  /**
   * List all the available workflows, keyed by md5 hash
   */
  function list() {
    list($code, $wfs) = $this->accept(200)->request('list');
    return $wfs;
  }


  function view($name_or_hash) {
    list($code, $workflow) = $this->accept(200)->request($name_or_hash);
    return $workflow;
  }


}


class WorkflowMismatchViolation extends CCError {
  static function create(...$params) :  ErrorInterface  {
    return new static(['hash' => $params[0]]);
  }
  function __toString() {
    return "The workflow hash '$this->hash' was not found on node $this->node";
  }
}