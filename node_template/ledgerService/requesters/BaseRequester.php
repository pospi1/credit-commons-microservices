<?php

/**
 * Reusable requester for one service local or remote.
 * Handle requests & responses from the ledger to the policy services using
 * file_get_contents.
 */
abstract class BaseRequester{

  /**
   * These would all be private if it weren't for the ClientRequester
   */
  protected $method = 'GET';
  protected $body;
  protected $timeout;
  protected $serviceUrl;
  protected $fullUrl;
  protected $fields = [];
  protected $queryParams = [];
  protected $acceptOnlyCodes = [];
  protected $httpOptions = ['ignore_errors' => TRUE];
  protected $host;
  protected $headers = [];

  /**
   *
   * @global string $node_url
   * @global int $timeout
   * @param LedgerAccount $downstreamAccount
   */
  function __construct(LedgerAccountRemote $downstreamAccount = NULL) {
    global $node_url, $config;
    if ($downstreamAccount) {
      $this->host = $downstreamAccount->url;
    }
    else {
      $this->host = $node_url;
    }
    // Need to construct the subdomain for the service.
    $this->serviceUrl = str_replace('//', '//'.static::SUBDOMAIN.'.', $this->host);
    $this->setTimeOut = $config['timeout'];
    // Currently we're not unzipping the response, and this wouldn't save much time anyway.
    //$this->setHeader('Accept-Encoding', 'gzip, deflate, br');
  }

  /**
   * Return a requester object
   * @param LedgerAccountRemote $dest
   *   Of the remote ledger
   * @return Requester
   */
  static function create(LedgerAccountRemote $dest = NULL) {
    static $cached;
    $class_name = get_called_class();
    if (!isset($cached)) {
      require_once 'requesters/'.$class_name.'.php';
      $cached = new $class_name($dest);
    }
    return $cached;
  }

  /**
   * Add the hash to the request header.
   * @param string $endpoint
   * @return array
   *   status code and body
   */
  protected function request(string $endpoint = '') :array {
    global $orientation, $node_url;

    // This should be something like.
    //SESS54b320f2c82b0edf66ad02d76e6f1ac0=dBzLNA90vlsJ_mSsBkSj2Y3UMV9oMOrNkGbfkU5EIAc
    //@todo I dont' think this Cookie is needed
    $this->setHeader('Cookie', $_SERVER['HTTP_COOKIE']);

    $this->setHeader('Accept', 'application/json');
    $this->setHeader('User-Agent', 'Credit-Commons');

    
    $orientation->localRequest = $this->host == $node_url;
    $this->buildUrl($endpoint);

    session_write_close();
    list ($code, $result_body, $message) = $this->doCurl($this->method, $this->fullUrl);
    session_start();

    $result = $this->handleRawResult($code, $result_body, $message);
    return $this->return($code, $result);
  }

  protected function handleRawResult($code, $result_body = NULL, string $message = '') {
    $result = json_decode($result_body);
    if ($result_body and $result_body != 'null' and is_null($result)) {
      cc_failure(BadJsonFailure::create($this->fullUrl, $result_body));
    }
    if ($code == 515) {
      // Just relay the response back upstream
      cc_failure($this->decodeJsonError($result));
    }
    if (!in_array($code, $this->acceptOnlyCodes)) {
      if (empty($message)) {
        switch ($code) {
          case 405:
            // This happens when the vhost doesn't exist
            $message = "Did you restart the web server after creating the credit commons nodes?";
            break;
          case 500:
            $message = "Check cc_error.log (at the repository root).";
            break;
          default:
            $message = "Unanticipated error";
        }
      }
      cc_failure(UnexpectedResultFailure::create($code, $this->fullUrl, $result_body, $message));
    }
    return $result;
  }

  /**
   * Add to the list of headers to output.
   * @param string $name
   * @param string $value
   * @return $this
   */
  protected function setHeader($name, $value) {
    // There's probably a max header length to truncate.
    $this->headers[$name] = $value;
    return $this;
  }

  /**
   * Store the request body
   * @param mixed $body
   * @return $this
   */
  public function setBody($body) {
    $this->body = $body;
    return $this;
  }

  /**
   * Add to the list of query parameters.
   * @param string $key
   * @param string $value
   * @return $this
   */
  public function addQueryParam(string $key, string $value) {
    $this->queryParams[$key] = $value;
    return $this;
  }
  /**
   * Assemble the query params and append them to the fullUrl.
   */
  protected function buildUrl($endpoint) {
    $this->fullUrl = $this->serviceUrl .'/'. $endpoint;
    if ($this->queryParams and $this->method = 'GET') {
      $this->fullUrl .= '?'.http_build_query($this->queryParams);
    }
  }

  /**
   * Store a field.
   * @param string $key
   * @param type $value
   * @return $this
   */
  protected function addField(string $key, $value) {
    $this->fields[$key] = $value;
    return $this;
  }

  /**
   * Set the REST request method.
   *
   * @param string $method
   * @return $this
   */
  protected function setMethod(string $method) {
    $this->method = strtoupper($method);
    return $this;
  }

  /**
   * Set the timeout. Should be longer for deeper trees.
   * @param int $secs
   * @return $this
   */
  protected function setTimeout(int $secs) {
    $this->timeout = $secs;
    return $this;
  }

  /**
   * Get the headers either as an array, or multiline string.
   * @param bool $implode
   * @return string
   */
  protected function getHeaders($implode = TRUE) {
    $temp = [];
    foreach ($this->headers as $key => $value) {
      $temp[] = $key.': '.$value;
    }
    if ($implode) {
      $temp = implode("\r\n", $temp);
    }
    return $temp;
  }

  /**
   * Which response codes to process. Others will throw an error.
   * @param array $args
   * @return $this
   */
  protected function accept(...$params) {
    foreach ($params as $v) {
      $this->acceptOnlyCodes[] = $v;
    }
    return $this;
  }

  /**
   * get the request body as json, either from $this->body or $this->fields
   * @return string
   */
  protected function getJsonBody() : string {
    $body = $this->body??$this->fields;
    $body = json_encode($body);
    if (strlen($body)) {
      $this->setHeader('Content-Type', 'application/json');
      $this->setHeader('Content-Length', strlen($body));
    }
    return $body;
  }

  /**
   * Reset this Requester, ready for reuse.
   */
  protected function return($code, $result = NULL, $comment = NULL) {
    global $timeout, $orientation;
    $this->timeout = $timeout;
    $this->queryParams = [];
    $this->headers = [];
    $this->body = '';
    $this->fields = [];
    $this->acceptOnlyCodes = [];
    // only for file_get_contents.
    $this->httpOptions = ['method' => 'GET', 'ignore_errors' => TRUE];
    $orientation->localRequest = NULL;
    return [$code, $result, $comment];
  }


  /**
   * Need to decide if it's better to request values individually by name.
   * @param array $vals
   */
  function getConfig() : array {
    list($code, $body) = $this->accept(200)
      ->request("config");
    return (array)$body;
  }

  /**
   * This function should be overridden to validate the $vals before submitting
   * them
   * @param array $vals
   * @return string[]|NULL
   *   Any error messages;
   */
  function setConfig(array $vals) {
    list($code, $body) = $this->setMethod('PATCH')
      ->setBody($vals)
      ->accept(200, 400)
      ->request("config");
    if ($code == 400 and $body) {
      return $body;
    }
  }

  /**
   * @param stdClass $v
   *   Violation object returned from downstream.
   * @return \Violation
   */
  protected function decodeJsonError(stdClass $v) : CCError {
    $messages = [];
    $class_name = $v->class;
    return new $class_name($v);
  }

  /**
   * Call another service using file_get_contents.
   * @param string $method
   * @param string $url
   * @return array
   *   the status code, and the response body.
   */
  protected function doFGC($method, $url) {
    $this->httpOptions['method'] = $this->method;
    $this->httpOptions['timeout'] = $this->timeout;
    if ($this->method != 'GET') {
      $this->httpOptions['content'] = $this->getJsonBody();
    }
    $this->httpOptions['header'] = $this->getHeaders(TRUE);
    $context = stream_context_create(['http' => $this->httpOptions]);
    // file_get_contents is a bit crude for http but has no dependencies
    $result_body = file_get_contents($this->fullUrl, false, $context);
    if ($http_response_header) {
      // in case of redirects, get the last header starting HTTP/1.1
      foreach (array_reverse($http_response_header) as $line) {
        if (preg_match('/HTTPS?\/1\.[0|1] ([0-9]{3}) ?(.*)/', $line, $matches)) {
          $status_code = $matches[1];
          $message = $matches[2];
          break;
        }
      }
    }
    else {
      cc_failure(DownstreamFailure::create($this->serviceUrl));
    }
    return [$status_code, $result_body, $message];
  }

  /**
   * @param string $method
   * @param string $url
   * @return array
   */
  protected function doCurl($method, $url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getHeaders(FALSE));
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->timeout);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    // This technique taken from https://stackoverflow.com/a/9434158
    // But only gets to the $_COOKIE in the second query;
    curl_setopt($ch, CURLOPT_COOKIE, 'sessionid='.session_id());
    //cc_log("Sending cookie to $url with sessionid: ".session_id());
    if ($method <> 'GET') {
      if ($method == 'POST') {
        curl_setopt($ch, CURLOPT_POST, 1);
      }
      else{
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
      }
      curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getJsonBody());
    }
    $result_body = curl_exec($ch);
    $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return [$status_code, $result_body, ''];
  }

}

