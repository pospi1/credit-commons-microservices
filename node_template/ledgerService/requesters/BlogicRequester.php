<?php

/**
 * Calls to the business logic service.
 */
class BlogicRequester extends BaseRequester {

  const SUBDOMAIN = 'blogic';

  /**
   * Add a new rule.
   *
   * @param Transaction $transaction
   * @return array
   */
  function appendto(Transaction $transaction) : array {
    list($code, $additional) = $this
      ->setBody($transaction->entries[0])
      ->setMethod('post')
      ->accept(200)
      ->request('append/'.$transaction->type);
    if ($num = count($additional)) {
      cc_message("Adding $num fees to transaction". " $this->serviceUrl");
    }
    return $additional;
  }

  /**
   * Get a list of the Blogic rules.
   *
   * @param bool $full
   */
  function getRules(bool $full = TRUE) : array {
    list (, $rules)  = $this->addQueryParam('full', $full)
      ->accept(200)
      ->request('rules');
    return (array)$rules;
  }
}
