<?php

/**
 * Handle the sending of transactions between ledgers and hashing.
 */
class TransversalTransaction extends Transaction {

  /**
   * {@inheritDoc}
   */
  public function saveTransactionNewVersion() {
    foreach (['payer', 'payee'] as $role) {
      $ledgerAccount = $this->{$role};
      if ($ledgerAccount instanceOf LedgerAccountRemote) {
        // Get the entries for the adjacent ledger, converted if rootwards
        $entries = $this->filterFor($ledgerAccount, $ledgerAccount instanceOf LedgerAccountBot);
        $$role = $this->getHash($ledgerAccount, $entries);
      }
    }
    $scribe = $_SESSION['user'];

    // The datestamp is added automatically
    $q = "INSERT INTO transactions (uuid, `version`, type, state, scribe, payee_hash, payer_hash) "
    . "VALUES ('$this->uuid', $this->version, '$this->type', '$this->state', '$scribe', '$payee', '$payer')";
    $new_id = Db::query($q);
    $this->writeEntries($new_id);
  }

  /**
   * Make a clone of the transaction with only the entries shared with an
   * adjacent ledger.
   *
   * @param LedgerAccountRemote $account
   */
  function filterFor(LedgerAccountRemote $account, $rootwards) : array {
    global $config;
    // Filter entries for the appropriate adjacent ledger
    // If this works we can delete all the TransversalEntry Classes.
    $remote_name = $account->localName;
    foreach ($this->entries as $e) {
      if ($e->payee->localName == $remote_name or $e->payer->localName == $remote_name) {
        $entries[] = $e;
      }
    }
    if ($rootwards and $config['bot_rate'] <> 1) {
      $entries = array_map(
        function ($e) {return $e->toRootNode();},
        $entries
      );
    }

    return $entries;
  }

  /**
   * Produce a hash of all the entries and transaction data in an easily repeatable way.
   * @param LedgerAccountRemote $ledgerAccount
   * @param array $entries
   * @return string
   */
  private function getHash(LedgerAccountRemote $ledgerAccount, array $entries) : string {
    global $config;
    foreach ($entries as $entry) {
      $str = round($entry->quant);// Intval avoids rounding errors. Might not be bulletproof!
      $str .= $entry->description;
      $rows[] = $str;
    }
    //cc_log($rows, $ledgerAccount->localName);

    $last_hash = $ledgerAccount->getLastHash();
    return static::makeHash(
      $last_hash,
      $this->uuid,
      $this->version,
      $rows
    );
  }

  /**
   * Compile the hash components and return the hash
   * @param string $last_hash
   * @param string $uuid
   * @param int $ver
   * @param float $diff
   *   A plus or minus, depending on if this is the payer or payee
   * @param array Entries
   *   An array of one string for each entry.
   * @return string
   *   The hash
   */
  private static function makeHash(string $last_hash, string $uuid, int $ver, $entries) {
    $entries_string = join('|', $entries);
    $string = join('|', [$last_hash, $uuid, $ver, $diff, $entries_string]);
    //cc_log($string, md5($string));
    return md5($string);
  }

  /**
   * Create a NEW transaction relayed from the upstream node.
   *
   * @param stdClass $input
   * @param bool $rootwards
   *   incoming entries. Could be from client or a flattened Entry. They should
   *   probably be validated either as they come in, or as converted to Entry
   *
   * @return \Transaction
   */
  public static function createFromUpstream(stdClass $input) : Transaction {
    global $orientation, $config;
    $upstreamName = $orientation->upstreamAccount->localName;

    $rows = (array)$input->entries;
    foreach ($rows as &$row) {
      $row->author = $upstreamName;
    }
    $entries = Transaction::createEntries($rows, FALSE);

    if ($orientation->upstreamIsRootwards() and $config['bot_rate'] <> 1) {
      $entries = array_map(
        function ($e) {return $e->fromRootNode();},
        $entries
      );
    }
    return new TransversalTransaction(
      $input->uuid,
      $input->version,
      $input->type,
      TransactionInterface::STATE_INITIATED,
      $entries
    );
  }

  /**
   * To send transactions to another node
   * - filter the entries
   * - new rows only upstream
   * - remove workflow
   * - remove actions
   *
   * @global Orientation $orientation
   * @return stdClass
   */
  public function jsonSerialize() : array {
    global $orientation;
    $adjacentAccount = $orientation->adjacentAccount();
    if ($adjacentAccount == 'client') {
      return parent::jsonSerialize();
    }
    $array = (array)$this;
    $array['entries'] = $this->filterFor($adjacentAccount, $orientation->goingRootwards());

    if ($orientation->goingUpstream()) {
      $array = array_filter($array['entries'], function($e) {return $e->isAdditional();} );
    }
    else {
      unset($array['actions'], $array['status'], $array['workflow'], $array['payeeHash'], $array['payerHash']);
    }
    return $array;
  }


  /**
   * {@inheritDoc}
   */
  function buildValidate() : void {
    global $orientation;
    parent::buildvalidate();
    if ($requester = $orientation->getDownstreamRequester()) {
      $requester->buildValidateTransaction($this);
    }
  }

  /**
   * {@inheritDoc}
   */
  function changeState(string $target_state) {
    global $orientation;
    if ($requester = $orientation->getDownstreamRequester()) {
      $requester->transactionChangeState($this->uuid, $target_state);
    }
    parent::changeState($target_state);
  }


}
