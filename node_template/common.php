<?php

error_reporting(E_ALL);
ini_set('allow_url_fopen', 1);

const USER_DEFAULT = '-auth-';
const USER_ADMIN = '-admin-';

/**
 *  The SERVER_NAME is the Virtualhost name i.e. microservice.nodename
 */
global $private, $service_name, $node_name, $inifile, $endpoint; //used in all the requesters
if (file_exists('../node.ini')) {
  $log_file = '../../devel.log';
  $message_file = '../../message.log';
  set_error_handler ('cc_log_error');
  $endpoint = arg(1);
  // @todo instead of using globals here, create getNodeSetting() and setNodeSetting()
  extract(parse_ini_file('../node.ini'));
  $node_name = basename(__DIR__);
  $dir = dirname($_SERVER['SCRIPT_FILENAME']);
  $service_name = strtolower(substr(basename($dir), 0, -7));
  $inifile = $service_name.'.ini';
  // Initialise service specific settings as global variables.
  if (file_exists($inifile)) {
    $config = parse_ini_file($inifile);
  }
  // The session is shared accross all the microservices in the node.
//session_name("some_session_name");
//session_set_cookie_params(0, '/', '.branch1');
//  if ($_COOKIE)cc_log($GLOBALS, 'received cookie');
//  else cc_log($GLOBALS, 'no cookie');
  if(isset($_COOKIE['sessionid'])){
    session_id($_COOKIE['sessionid']);
  }
session_start();
}
else {
  $log_file = '../devel.log';
  $message_file = '../message.log';
}

//Handle any incoming config requests, same for every service at the moment.
$method = strtoupper($_SERVER['REQUEST_METHOD']);
if ($endpoint == 'config') {
  if ($method == 'GET') {
    global $config;
    cc_response(200, $config);
  }
  elseif ($method == 'PATCH') {
    $vals = (array)credcom_json_input();
    foreach ($vals as $key => $val) {
      $messages[] = saveConfig($key, $val);
    }
    if ($result = array_filter($messages)) {
      cc_response(400, $result);
    }
    cc_response(200);
  }
}

/**
 *
 * @global string $service_name
 * @param string $key
 * @param string $value
 */
function saveConfig($key, $value) {
  global $inifile;
  $lines = file($inifile, FILE_IGNORE_NEW_LINES);
  setConfig($lines, $key, $value);
  file_put_contents($inifile, implode("\n", $lines));
}

function setConfig(&$lines, $key, $value) {
  foreach ($lines as &$line) {
    if (stripos($line, $key) === 0) {
      $line = strtolower($key)." = '$value'";
      return;
    }
  }
}

/**
 *
 * @param int $index
 * @return mixed
 */
function arg(int $index) {
  static $args;
  // This is clunky but works with my settings for both apache and nginx redirects
  $uri = $_SERVER['REDIRECT_URL']??$_SERVER['REQUEST_URI']??'';
  if (strpos($uri, '?')) {
    $uri = substr($uri, 0, strpos($uri, '?'));
  }
  if (!isset($args)) {
    $args = array_values(explode('/', $uri));
  }
  // Ensure $arg[0] is always empty
  if ($index > 0) {
    return $args[$index] ?? NULL;
  }
  //$arg[1] is always the endpoint
  elseif ($index == -1) {
    //Provide additional args after the endpoint
    return array_slice($args, 2);
  }
  else {
    trigger_error('Unacceptable argument to arg() function: '.$index);
  }
}

/**
 * Send the result back.
 *
 * Body is always a json object
 * @param int $code
 * @param array|stdClass $body
 *   The payload
 * @param string $message
 *   A very short message, for the header.
 */
function cc_response(int $code, $body = NULL, string $message = '') {
  header($_SERVER["SERVER_PROTOCOL"] ." $code ". str_replace("'", "", $message));
  $content = '';
  if (!is_null($body)) {
    if (is_string($body)) {
      header('Content-Type: application/text');
      $content = $body;
    }
    else {
      header('Content-Type: application/json');
      $content = json_encode($body); // second param of
    }
    header('Content-Length', strlen($content));
    print $content;
  }
  cc_log_endpoint($code, $content);
  exit;
}


function authorise() {
  global $roles;
  return TRUE;
}

interface TransactionInterface {
  const STATE_INITIATED = 'init'; //internal use only
  const STATE_VALIDATED = 'validated';
  const STATE_PENDING = 'pending';
  const STATE_COMPLETED = 'completed';
  const STATE_ERASED = 'erased';
  const STATE_TIMEDOUT = 'timedout';
}


/**
 * Get the request body json and convert it to php.
 */
function credcom_json_input() {
  $json = file_get_contents('php://input');
  if (!$json) {
    global $method;
    cc_response(400, "Body missing $method from request.");
  }
  if ($body = json_decode($json)) {
    return $body;
  }
  cc_log($json, 'Badly formed json body');
  cc_response(400, $json, 'Bad json');
}

/**
 * Error handler.
 * @todo make separate error handler for the ledger service and all the others
 * should just cc_log because they don't have a database.
 */
function cc_log_error(int $errno , string $errstr, string $errfile, int $errline) {
  if (strpos($errstr, 'file_get_contents') === 0) {
    return 'cc_log_error';
  }
  $str = $errfile.' line '.$errline;
  //this works on apache...
  //@list($message, $trace) = explode('Stack trace:', $errstr);
  switch ($errno) {
    case E_USER_NOTICE:
    case E_USER_WARNING:
    case E_USER_DEPRECATED:
      cc_log($str);
      if (function_exists('ledger_journal')) {
        ledger_journal($errstr, 'warning');
      }
      break;
    case E_ERROR:
    case E_USER_ERROR:
    case E_PARSE:
    case E_CORE_ERROR:
    case E_COMPILE_ERROR:
    case E_RECOVERABLE_ERROR:
      cc_log($str);
      if (function_exists('cc_failure')) {//ledger service only
        cc_failure(
          PhpFailure::create(
            $errno,
            $_SERVER['REQUEST_URI'],
            file_get_contents('php://input'),
            $errstr
          )
        );
      }
  }
}


/**
 * An error system for passing errors back upstream and translating them at the end
 */
interface  ErrorInterface {
  /**
   * Register a new error when it occurs
   * @return \ErrorInterface
   *
   * @note Commented out because it conflicts, for some reason with the instantiations
   */
  static function create(...$params) :  ErrorInterface ;

}

abstract class CCError implements ErrorInterface {
  public $node;
  public $class;

  function __construct($values = []) {
    global $node_name;
    // these will be overwritten when the violation is being passed from node to node
    $this->node = $node_name;
    $this->class = get_called_class();
    foreach ($values as $key => $val) {
      $this->$key = $val;
    }
  }

  static function create(...$params) :  ErrorInterface {
    return new static();
  }

}


class BadJsonFailure extends CCError {
  public $url;
  public $response;
  static function create(...$params) :  ErrorInterface  {
    return new static(['url' => $params[0], 'response' => $params[1]]);
  }
  function __toString() {
    return "$this->url returned the badly formatted json: ".$this->response;
  }
}

class MiscFailure extends CCError {
  public $message;
    static function create(...$params) : ErrorInterface  {
    return new static(['message' => $params[0]]);
  }
  function __toString() {
    return $this->message;
  }
}

/**
 * Pass $id
 */
class UnexpectedResultFailure extends CCError {
  public $code;
  public $url;
  public $content;
  public $english;

  static function create(...$params) :  ErrorInterface  {
    return new static(['code' => $params[0], 'url' => $params[1], 'content' => $params[2], 'english' => $params[3]]);
  }

  function __toString() {
    $message = "$this->node $this->url returned ".$this->code;
    if ($this->content) {
      $json = json_encode(json_decode($this->content), JSON_PRETTY_PRINT);
      $message .= "<br />The request body was <pre>$json</pre>";
    }
    return $message;
  }
}

class DownstreamFailure extends CCError {
  public $url;
  static function create(...$params) :  ErrorInterface  {
    return new static(['url' => $params[0]]);
  }
  function __toString() {
    return "$this->url was inaccessible from $this->node";
  }
}


class PhpFailure extends CCError {
  public $code;
  public $url;
  public $content;
  public $details;

  static function create(...$params) :  ErrorInterface  {
    return new static(['code' => $params[0], 'url' => $params[1], 'content' => $params[2], 'details' => $params[3]]);
  }

  function __toString() {
    return "$this->node $this->url returned ".$this->convertCode($this->code);
  }

  function convertCode($code) : string {
    switch($code) {
      case 1:
        return 'E_ERROR';
      case 4:
        return 'E_PARSE';
      case 16:
        return 'E_CORE_ERROR';
      case 64:
        return 'E_COMPILE_ERROR';
      case 256:
        return 'E_USER_ERROR';
      case 4096:
        return 'E_RECOVERABLE_ERROR';
      default:
        return 'unidentified error code '.$code;
    }
  }
}


/**
 * The following functions are for development - they write to local files to be
 * displayed by the ccclient.
 */

/**
 * Write to the log file
 *
 * @param mixed $data
 * @param string comment
 */
function cc_log($arg) : void {
  global $log_file;
  $args = func_get_args();
  $lines = [];
  $data = array_shift($args);
  $comment = array_shift($args);
  $lines[] = "\n<strong>".$_SERVER['SERVER_NAME'] .': '.$_SERVER['REQUEST_METHOD'] .' '.$_SERVER['REQUEST_URI']."</strong>\n". print_r($comment, 1);
  if (!is_string($data)) {
    $data = "\n<pre>".print_r($data, 1).'</pre>';
  }
  $lines[] = $data;
  if ($called = debug_backtrace()[0]) {
    if (isset($called['class'])) {
      $source = $called['class'].'::'.$called['function'].'() line '.$called['line'];
    }
    else {
      $source = $called['file']??''.'::'.$called['function']."()";
    }
    $lines[] = "\n<em>Called in ".$source." line ".$called['line']."</em>";
  }
  @file_put_contents($log_file, implode("\n<br />", $lines)."\n\n<hr />", FILE_APPEND);
}

/**
 *
 * @global string $endpoint
 * @global string $node_name
 * @param int $code
 * @param string $body
 * @return void
 */
function cc_log_endpoint(int $code, string $body = NULL) : void {
  global $service_name, $endpoint, $node_name;
  if (empty($node_name)){
    // because this function could be called by ccclient.
    return;
  }
  $file = "../../$service_name.api.log";
  if ($content = file_get_contents($file)) {
    $content = unserialize($content);
  }
  else {
    $content = [];
  }
  global $method;
  switch ($method) {
    case 'GET':
    case 'HEAD':
    case 'DELETE':
      $content[$endpoint][$method]['request'] = arg(-1);
      break;
    case 'POST':
      $content[$endpoint][$method]['request'] = cc_log_endpoint_request_body();
      break;
    case 'PATCH':
      $content[$endpoint][$method]
      ['request'] = cc_log_endpoint_request_body();
      break;
    default:
      cc_log('unknown REST verb: '.$_SERVER['REQUEST_METHOD']);
  }
  $content[$endpoint][$method]['response'][$code] = htmlentities($body);
  file_put_contents($file, serialize($content));
}

function cc_log_endpoint_request_body() {
  if ($json = file_get_contents('php://input')) {
    return json_decode($json);
  }
}


/**
 * Log a system message to be printed by the client
 * @global type $node_name
 * @param string $message
 * @todo separate this out in case there is no client.
 */
function cc_message(string $message) {
  global $node_name, $message_file;
  $messages = unserialize(@file_get_contents($message_file));
  if (!strlen($message)) {
    $messages = [];
  }
  else {
    $prefix = ($node_name??'CLIENT').": ";
    $prefix .= basename(dirname($_SERVER['SCRIPT_FILENAME'], 1.)).": ";
    $messages[] = $prefix . htmlentities($message);
  }
  file_put_contents($message_file, serialize($messages));
}


register_shutdown_function('shut');
function shut(){
  $error = error_get_last();
  if($error && ($error['type'] & E_ERROR | E_USER_ERROR | E_PARSE | E_CORE_ERROR |
        E_COMPILE_ERROR | E_RECOVERABLE_ERROR)){
     cc_log_error($error['type'], $error['message'], $error['file'], $error['line']);
  }
}