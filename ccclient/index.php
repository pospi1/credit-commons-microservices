<?php
require 'setup.php'; //prepares the $nodes array, $active_node
$demo_mode = $_SERVER['REQUEST_SCHEME'] == 'https';
if (empty($nodes)) {
  header('Location: makenodes.php');
  exit;
}
if (isset($_GET['client'])) {
  $active_node = $nodes[$_GET['client']];
}
elseif (count($nodes) == 1) {
  $active_node = reset($nodes);
}
if (!$demo_mode and isset($_GET['serverconf'])) {
  $server = ServerConfigurer::create();
  $conf = $server->setup();
  clientAddInfo($conf);
}
elseif (!$demo_mode and isset($_GET['truncate'])) {
  foreach ($nodes as $node) {
    $node->truncate();
  }
  clientAddInfo('Transactions deleted');
}
elseif (!$demo_mode and isset($_GET['rebuild'])) {
  foreach ($nodes as $node) {
    $node->makedb($node->getDbName());
  }
}
?><!DOCTYPE html>
<html lang="en">
  <head>
    <title>Credit Commons client</title>
    <link type="text/css" rel="stylesheet" href="style.css" media="all">
    <script type="text/javascript" src="script.js"></script>
  </head>
  <body bgcolor="fafafa">
  <?php if (!$demo_mode): ?><a href="makenodes.php" title="To make a new node you must be able to create a new virtualHost on this server"/>New node</a><?php endif;?>
<?php if (count($nodes) > 1) : ?>
  <a href="index.php" title="An overview of all the nodes hosted here."/>View charts</a>,
or <span title="This client works with any of the nodes installed alongside it, one at a time">Connect to node:</span>
<span title="To see the tree relationships between the nodes, click View charts"></span>
  <?php

  foreach (array_keys($nodes) as $node_name) {
    $checked = (isset($active_node) && $active_node->name == $node_name) ? 'checked' : '';
    print "\n".'<input type="radio" name="client" value="'.$node_name.'" '.$checked .' onclick="window.location=\'index.php?client='.$node_name.'\'"/>'.$node_name;
  }

endif;?>
  <hr />
<?php if (!isset($active_node)) {
  print_all_charts();
  exit;
}
$policyRequester = $active_node->getRequester('policy');
$ledgerRequester = $active_node->getRequester('ledger');
$local_account_names = $policyRequester->filter(['local' => 1], 'nameonly');
print loginOptions($local_account_names);
?>
<hr />
<?php
if (empty($_GET['acc'])){
  exit;
}
$activeService = '';

if (isset($active_node)) {
  global $errorfield;
  $services = [
    'ledger' => ['createTransaction', 'stateChange','filterTransaction', 'viewAccounts', 'accountSummary', 'showHistory', 'ledgerConfig', 'getWorkflows', 'handshake'],
    'policy' => ['fetchPolicy', 'policyFilter', 'signUp', 'overridePolicy', 'policyConfig'],
    'blogic' => ['doBlogic', 'blogicConfig']
  ];
  $activeService = key($services);
  foreach ($services as $service => $methods) {
    foreach ($methods as $method) {
      if (isset($_POST[$method])) {
        $activeService = $service;
        break 2;
      }
    }
  }
  unset($_REQUEST['client']);
  if (empty($_REQUEST)) {
    // Check that connected nodes are active.
    list($code, $results) = $active_node->getRequester('ledger')->handshake();
    if ($code == 200) {
      if (!empty($results[409])) {
        foreach ($results[409] as $node) {
          setResponse("Integrity problem connecting to node $node", 'red');
        }
        unset($results[409]);
      }
      if (!empty($results[200])) {
        clientAddInfo("Successfully connected to nodes: ".implode(', ', $results[200]));
        unset($results[200]);
      }
      if (!empty($results)) {
        foreach (reset($results) as $node) {
          setResponse("Unable to reach $node", 'red');
        }
      }
    }
    else {
      setResponse("Backend unavailable", 'red');
    }
  }

  extract($_POST);
  if ($activeService == 'ledger') {
    if (isset($filterTransaction)) {
      $transactions = $ledgerRequester->print()->filter($_REQUEST);
      if (!empty($_REQUEST['full'])) {
        clientAddInfo(makeTransactionSentences($transactions));
        // not this because it puts a form within a form
        setResponse(makeTransactionSentences($transactions), 'green');
      }
      else {
        clientAddInfo(implode('<br />', $transactions));
        setResponse(implode('<br />', $transactions), 'green');
      }
    }
    elseif(isset($viewAccounts)) {
      $accounts = $ledgerRequester->print()->accounts(isset($details), isset($deep)?TRUE:FALSE);
      setResponse($accounts);
    }
    elseif (isset($accountSummary)) {
      $name = $_POST['acc_name'];
      if (list($name, $stats) = $ledgerRequester->print()->getStats($name)) {
        setResponse(formatStats($name, $stats), 'green');
      }
      else {
        setResponse("Unable to retrieve stats for '$name'", 'red');
      }
    }
    elseif(isset($showHistory)) {
      // Get the balances and times.
      $name = $_POST['acc_name'];
      $history = $ledgerRequester->print()->getHistory($name);
      if (count($history) > 2) {
        setResponse($history, 'green');
      }
      else {
        setResponse("No history for '$name'", 'green');
      }
    }
    elseif (isset($stateChange) and isset($uuid)) {
      //Permission is assumed, at least here in the client.
      if ($active_node->getRequester('ledger')->print()->transactionChangeState($uuid, $stateChange)) {
        clientAddInfo('Transaction saved');
      }
    }
    elseif (isset($getWorkflows)) {
      $wf = $ledgerRequester->getWorkflows();
      setResponse((array)$wf, 'green');
    }
    elseif ($demo_mode and isset($ledgerConfig)) {
      credcom_config_save('ledger');
    }
    elseif (isset($handshake)) {
      list($code, $shakes) = $ledgerRequester->handshake();
      setResponse($shakes, 'green');
    }
  }
  elseif ($_POST && $activeService == 'policy') {
    if (isset($signUp)) {
      //ApplytoJoin
      if ($accountPolicy = $policyRequester->print()->join($name)) {
        setResponse("'$name' joined ledger '$active_node->name'", 'green');
      }
      else {
        setResponse("Could not create account '$name' on ledger '$active_node->name'", 'red');
      }
    }
    elseif (isset($fetchPolicy) and $acc_id) {
      if ($accountPolicy = $policyRequester->print()->fetch($acc_id)) {
        setResponse($accountPolicy, 'green');
      }
      else {
        setResponse("No policy with name: $acc_id", 'red');
      }
    }
    elseif(isset($policyFilter) && isset($chars)) {
      if ($policyFilter == 'Filter') {
        $filters['chars'] = $chars;
        if ($acc_status == 'blocked') {
          $filters['status'] = 0;
        }
        elseif ($acc_status == 'active') {
          $filters['status'] = 1;
        }
        if ($filteredPolicies = $policyRequester->print()->filter($filters, $fullPolicy)) {
          setResponse($filteredPolicies, 'green');
        }
        else {
          setResponse("No policies met those criteria");
        }
      }
    }
    elseif (isset($overridePolicy) and isset($acc_id)) {
      $params = [];
      if (is_numeric($min)) {
        $params['min'] = $min;
      }
      if (is_numeric($max)) {
        $params['max'] = $max;
      }
      if ($acc_status != 'any') {
        $params['active'] = (int)$acc_status == 'active';
      }
      if ($url) {
        $params['url'] = $url;
      }
      if ($params) {
        $policy = $policyRequester->print()->override($acc_id, $params);
        setResponse($policy, 'green');
      }
      else {
        setResponse("No values given to override $acc_id", 'red');
      }
    }
    elseif ($demo_mode and isset($policyConfig)) {
      credcom_config_save('policy');
    }
  }
  elseif ($_POST and isset($doBlogic)) {
    $bLogicRequester = $active_node->getRequester('blogic');
    if(isset($example_transaction)) {
      if ($data = json_decode($example_transaction)) {
        $type = $data->type;
        unset($data->type);
        $transaction = [
          'type' => $type,
          'entries' => [
            $data
          ]
        ];
        if ($result = $bLogicRequester->_appendTo((object)$transaction)) {
          setResponse($result);
        }
        else {
          setResponse('No additional entries were added.');
        }
      }
      else {
        clientAddError('Badly formed json object');
      }
    }
    elseif ($demo_mode and isset($blogicConfig)) {
      credcom_config_save('blogic');
    }
    else {
      setResponse('No business logic configured for this node', 'green');
    }
  }

 } ?>

<?php print topTransactions(); ?>
  <?php if (isset($active_node)) : ?>
    <?php print showInfo();
    if ($m = unserialize(@file_get_contents($message_file))) : ?>
  <!-- print messages before generating the forms so we only get the messages from the user's request -->
  <a class="messages collapsible" title="Contents of cc_message() function throughout the reference implementation">Under the hood</a>
  <div class="collapsible-content"><?php print implode("\n<br />", $m); ?></div>
  <?php endif; ?>
    <?php
    $log = get_devel_log();
    print generate_all_forms($services, $activeService) ?>
  <?php endif; ?>
  <?php print $log; ?>
  <?php print get_api_log($services); ?>
    <script>
var coll = document.getElementsByClassName("collapsible");
var i;
for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.display === "block") {
      content.style.display = "none";
    } else {
      content.style.display = "block";
    }
  });
}</script>
    <?php if (!$demo_mode) : ?>
    <p><a href="?serverconf">Server conf</a>
    <br /><a href="?truncate">Truncate Dbs</a>
    <br /><a href="?rebuild">Rebuild all dbs</a></p>
    <?php endif; ?>
  </body>
</html><?php
@unlink($message_file);

/**
 *
 * @param Transaction[] $transactions
 * @return string
 */
function makeTransactionSentences(array $transactions) :string {
  // This assumes the entries are in order with branchward nodes following rootward nodes.
  // It is probably easier to handle if arrays of entries are returned
  $output = '';
  $template = '<div class = "@class @state">@payer will pay @payee @quant for \'@description\' @state @links</div>';
  $search = ['@class', '@payee', '@payer', '@quant', '@description', '@state', '@links'];
  foreach ($transactions as $transaction) {
    $links = !empty($transaction->actions) ? ' - '.credcom_action_links($transaction->uuid, (array)$transaction->actions) : '';
    // put the links next to the first entry.
    $first = TRUE;
    foreach ($transaction->entries as $entry) {
      $replace = [
        $first ? "primary" : "dependent",
        $entry->metadata->{$entry->payee}??$entry->payee,
        $entry->metadata->{$entry->payer}??$entry->payer,
        $entry->quant,
        $entry->description,
        $first ? "($transaction->state)" : '',
        $first ? $links : '',
      ];
      $output .= str_replace($search, $replace, $template);
      $first = FALSE;
    }
  }
  return $output;
}

/**
 * Render the transaction action links as forms which can post
 * @param string $uuid
 * @param array $actions
 * @return string
 */
function credcom_action_links(string $uuid, array $actions) {
  $output[] = '<form method="post" class="inline" action=>';
  $output[] = '<input type="hidden" name="uuid" value="'.$uuid.'">';
  foreach ($actions as $target_state => $title) {
    $output[] = '<button type="submit" name="stateChange" value="'.$target_state.'" class="link-button">'.$title.'</button>';
  }
  $output[] = '</form>';
  return implode($output);
}

function formatStats($name, $data) {
  $output = '<table border=1><thead><tr><th>'.$name.'</th><th>Actual</th><th>Pending</th></tr></thead>';
  $output .='<tbody><tr><th>Balance</th><td>'.$data->completed->balance.'</td><td>'.$data->pending->balance.'</td></tr>';
  $output .='<tr><th>Trades</th><td>'.$data->completed->trades.'</td><td>'.$data->pending->trades.'</td></tr>';
  $output .='<tr><th>Entries</th><td>'.$data->completed->entries.'</td><td>'.$data->pending->entries.'</td></tr>';
  $output .='<tr><th>Volume</th><td>'.$data->completed->volume.'</td><td>'.$data->pending->volume.'</td></tr>';
  $output .='<tr><th>Gross Income</th><td>'.$data->completed->gross_in.'</td><td>'.$data->pending->gross_in.'</td></tr>';
  $output .='<tr><th>Gross Expenditure</th><td>'.$data->completed->gross_out.'</td><td>'.$data->pending->gross_out.'</td></tr>';
  $output .='<tr><th>Partners</th><td>'.$data->completed->partners.'</td><td>'.$data->pending->partners.'</td></tr>';
  return $output.'</tbody></table>';
}

function generate_all_forms($services, $activeService) {
  $tabs = '<br/><div style ="display:inline-block; padding: 0.5em; clear:both;" title="Microservices were chosen not for performance but for modularity. The ledger is the main workhorse ,and the only one which connects between nodes. The others are only called by the ledger.">Microservices >></div>';
  $tab_block = '';
  foreach ($services as $service => $methods) {
    $classes = ["bigtab"];
    if ($activeService == $service) {
      $classes[] = 'front';
    }
    $onclick = "openTab(event,'.bigtab','.service','$service')";
    $tabs .= '<div class ="'.implode(' ',$classes).'" onclick="'.$onclick.'">'.$service. ($service == 'workflow'?' (deprecated)':'').'</div>';
    $tab_block .= div_print($service, $methods, $activeService == $service);
  }
  return $tabs . $tab_block;
}

/**
 * Render a service as a tab
 * @param type $service
 * @param array $methods
 * @param bool $is_front_service
 */
function div_print($service, array $methods, bool $is_front_service) {
  $attributes['id'][] = $service;
  $attributes['class'][] = 'service';
  if ($is_front_service) {
    $attributes['class'][] = 'front';
  }

  foreach ($methods as $method) {
    if (isset($_POST[$method])) {
      $attributes['class'][] = 'front';
      $front_op = $method; break;
    }
  }
  if (!isset($front_op)) {
    $front_op = $methods[0];
  }
  foreach ($methods as $method) {
    $output[] = get_method_form($method, $front_op==$method);
    $but_att = [
      'onclick' => ["openTab(event,'#$service .littletab','#$service .method','$method')"],
      'class' => ['littletab'],// front?
    ];
    if ($front_op==$method) {
      $but_att['class'][] = 'front';
    }
    $buttons[] = getDivTag($but_att, $method);
  }

  array_unshift($buttons, '<div style="display:inline-block; padding: 0.3em; font-size:85%;" title="These are the names of the endpoints corresponding to the openapi documentation">Methods >></div>');
  return getDivTag($attributes, implode($buttons).implode("\n", $output));
}

/**
 * Render a method form as a subtab
 * @param type $method
 * @param type $is_front
 * @return type
 */
function get_method_form($method, $is_front) {
  $output = [];
  MakeForm::$method($output);
  $fields = implode($output);
  $attributes['id'][] = $method;
  $attributes['class'][] = 'method';
  if ($is_front) {
    $attributes['class'][] ='front';
  }
  if ($method == 'stateChange') {
    return getDivTag($attributes, $fields);
  }
  else {
    return getFormTag($attributes, $fields);
  }
}

class MakeForm {

  static function createTransaction(&$output) {
    global $uuid, $createTransaction, $payer, $payee, $quant, $description, $errorfield, $ledgerRequester, $type;

    $output[] = '<h3>Register transaction</h3>';
    if ($createTransaction) {
      $output[] = $response = getResponse();
    }
    $workflows = $ledgerRequester->getWorkflows();
    $output[] = '<select name = "type">';
    // Show only the top level workflows, for simplicity
    foreach(reset($workflows) as $workflow) {
      $selected = $type == $workflow->id ? 'selected="selected"' : '';
      $output[] = '<option value="'.$workflow->id.'" '.$selected.'>'.$workflow->label . ': '.$workflow->summary.'</option>';
    }
    $output[] = '</select>';
    $output[] = '<p>Create a transaction (as '.$_GET['acc'].')</p>';
    // Different account select widgets depending on if the node is isolated.
    $output[] = '<label class=required>Payee</label>';
    $output[] = selectAccount('payee', $payee??'alice', $errorfield == 'payee' ? "error" :'').'<br />';
    $output[] = '<label class=required>Payer</label>';
    $output[] = selectAccount('payer', $payer??'bob', $errorfield == 'payer' ? "error" : '').'<br />';

    $output[] = '<label class=required>Quantity</label>';
    $output[] = '<input name = "quant" type="number" placeholder = "0" value="'.($quant ?? '10') .'" class="'.($errorfield == 'quant' ? "error" : '').'" /><br />';
    $output[] = '<label>Description</label>';
    $output[] = '<input name = "description" type="text" placeholder="blah blah" value="'.($description ?? 'blah blah').'" /><br />';
    $output[] = '<input type = "submit" name = "createTransaction" value="Create" />';
  }

  static function stateChange(&$output) {
    global $ledgerRequester, $stateChange;
    if ($pending = $ledgerRequester->filter(['state'=> 'pending', 'full' => 1])) {
      $output[] = '<h4>Incomplete Transactions:</h4>';
      foreach ($pending as $t) {
        // show only the pending transactions the current user doesn't have to sign.
        // Transactions which require action are shown at the top
        if (!isset($t->actions->completed)) {
          $output[] = makeTransactionSentences([$t]);
        }
      }
    }
    if ($transactions = $ledgerRequester->filter(['full' => 1, 'state' => 'completed'])) {
      $output[] = '<h4>Completed Transactions:</h4>';
      $output[] = makeTransactionSentences($transactions);
    }
    // Get all the transactions on the ledger
    if ($transactions = $ledgerRequester->filter(['full' => 1, 'state' => 'erased'])) {
      $output[] = '<h4>Erased Transactions:</h4>';
      $output[] = makeTransactionSentences($transactions);
    }
    if (!$output) {
      $output[] = 'No transactions in the system yet...';
    }
  }


  static function filterTransaction(&$output) {
    global $payer, $payee, $involving, $uuid, $state, $before, $after, $description, $full, $filterTransaction;
    $output[] = '<h3>Filter Transactions</h3>';
    $output[] = '<p>See any transaction on this node (no access control for members)</p>';
    $output[] = '<br /><label>Payee</label>';
    $output[] = '<input type="textfield" name="payee"/>';
    $output[] = '<br /><label>Payer</label>';
    $output[] = '<input type="textfield" name="payer""/>';
    $output[] = '<br /><label>Either payee or payer</label>';
    $output[] = '<input type="textfield" name="involving" value="'.$involving.'"/>';
    $output[] = '<br /><label>UUID</label>';
    $output[] = '<input name = "uuid" type="text" placeholder="71d6bdcb-0b9c-442f-b6bb-01f3a5f95def" value="'.($uuid ?? '').'" />';
    $output[] = '<br /><label>state</label>';
    $output[] = '<select name = "state">';
    $output[] = '  <option value="">-- Any --</option>';
    $output[] = '  <option value="completed" '.($state == 'completed' ? "selected" : '').'>Completed</option>';
    $output[] = '  <option value="pending" '.($state == 'pending' ? "selected" : '').'>Pending</option>';
    $output[] = '  <option value="erased" '.($state == 'erased' ? "selected" : '').'>Erased</option>';
    $output[] = '  <option value="validated" '.($state == 'validated' ? "selected" : '').'>Validated</option>';
    $output[] = '  <option value="timedout" '.($state == 'timedout' ? "selected" : '').'>Timed out</option>';
    $output[] = '</select>';
    $output[] = '<br /><label>Date Before</label>';
    $output[] = '<input type="date" name="before" value = "'.$before.'"/>';
    $output[] = '<br /><label>Date After</label>';
    $output[] = '<input type="date" name="after" value = "'.$after.'"/>';
    $output[] = '<br /><label>Description</label>';
    $output[] = '<input type="textfield" name="description" />';
    $output[] = '<br /><label>Show full transaction</label>';
    $output[] = '<input type="checkbox" name="full" '. ($full??'checked').'"/>';
    $output[] = '<br /><input type = "submit" name = "filterTransaction" value="View"/>';
    $output[] = ' (a few more filters may be documented in the API.)';
  }

  static function viewAccounts(&$output) {
    global $details, $deep, $accounts, $active_node;
    $output[] = '<h3>View accounts</h3>';
    $output[] = '<p>This part of the API and code need work to yield a nice usable tree.</p>';
    if ($accounts) {
      $output[] = get_one_balance_chart($active_node);
      $output[] = getResponse();
    }
    $output[] = '<p>Any member can see the accounts on this ledger and all the rootwards ledgers.</p>';
    $output[] = '<input type = "checkbox" name = "details" value = "1" '.($details ? 'checked' : '').' />Show account details and handshake any connected nodes<br />';
    $output[] = '<input type = "checkbox" name = "deep" value = "1" '.($deep ? 'checked' : '').' />Show the node tree, from this node to the trunk.<br />';
    $output[] = '<p>N.B. The API allows querying branchwards ledgers, but they might be private.</p>';
    $output[] = '<input type = "submit" name = "viewAccounts" value = "Account tree"/><br />';
  }

  static function accountSummary(&$output) {
    global $acc_name, $accountSummary;
    $output[] = '<h3>View account</h3>';
    $output[] = '<p>Trading summary for an account</p>';
    if ($accountSummary){
      $output[] = getResponse();
    }
    $output[] = '<label class=required >Account name or path</label>';
    $output[] = selectAccount('acc_name', $acc_name).'<br />';
    $output[] = 'Relative paths to accounts on rootwards nodes (@todo + open branchwards nodes)';
    $output[] = '<br /><br /><input type = "submit" name = "accountSummary" value = "Show Account" />';
  }

  static function showHistory(&$output) {
    global $acc_name, $showHistory, $history;
    $output[] = '<h3>View account history</h3>';
      if (@count($history) > 2) {
        $output[] = get_one_history_chart($acc_name, $history);
      }
    $output[] = '<p>A list of balances and times, starting with account creation.</p>';
    if ($showHistory){
      $output[] = getResponse();
    }
    $output[] = '<label class=required >Account name or path</label>';
    $output[] = selectAccount('acc_name', $acc_name).'<br />';
    $output[] = '<br /><input type = "submit" name = "showHistory" value = "Show History" />';
  }

  static function ledgerConfig(&$output) {
    $output[] = '<h3>Config</h3>';
    static::renderConfig('ledger', $output);
  }

  static function policyConfig(&$output) {
    $output[] = '<h3>Policy Config</h3>';
    static::renderConfig('policy', $output);

  }
  static function bLogicConfig(&$output) {
    $output[] = '<h3>Business logic config</h3>';
    $output[] = '<p>Takes a fixed or proportional amount from every transaction.<br />';
    static::renderConfig('blogic', $output);

  }
  static function workflowConfig(&$output) {
    $output[] = '<h3>Workflow config</h3>';
    static::renderConfig('workflowEdit', $output);
  }

  private static function renderConfig($requester_class, &$output) {
    global $active_node, $demo_mode;
    $values = $active_node->getRequester($requester_class)->getConfig();
    if ($values) {
      foreach ($values as $key => $value) {
        $output[] = "<br />$key <input name=\"$key\" value=\"$value\" ".($demo_mode ?'disabled':'')." >";
      }
      // Quick way to get the calling functionname.
      $name = $requester_class.'Config';
      $output[] = '<br /><input type = "submit" name = "'.$name.'" value = "Set Config" '.($demo_mode ? 'disabled' : '').'/>';
      $output[] = '<br />This reference implementation does not support datatypes for config variables so handle with care!';
    }
    else {
      $output[] = '<br />No config for this service';
    }
  }

  static function signUp(&$output) {
    global $signUp, $id;
    $output[] = '<h3>Create a new account</h3>';
    $output[] = '<p>Only the name is required. Balance limits will be set to default</p>';
    if ($signUp) {
      $output[] = getResponse();
    }
    $output[] = '<label class=required>Id</label>';
    $output[] = '<input name = "name" type="text" placeholder="jack123" value="'.($id ?? '').'" /><br />';
    $output[] = '<input type = "submit" name = "signUp" value="Join" />';
  }
  static function fetchPolicy(&$output) {
    global $accountPolicy, $acc_id, $fetchPolicy;
    $output[] = '<h3>Fetch an existing account policy</h3>';
    if ($fetchPolicy){
      $output[] = getResponse();
    }
    $output[] = '<p>Everything the policy service knows about the account.';
    $output[] = '<br /><label class=required >Account name</label><br />';
    $output[] = selectAccount('acc_id', $acc_id).'<br />';
    $output[] = '<input type = "submit" name = "fetchPolicy" value="Fetch" />';
  }

  static function overridePolicy(&$output) {
    global $min, $max, $acc_id, $acc_status, $overridePolicy;
    $output[] = '<h3>Alter account details.</h3>';
    $output[] = '<p>Leave min/max blank to revert to node defaults</p>';
    if ($overridePolicy) {
      $output[] = getResponse();
    }
    $output[] = '<label class=required >Account name</label><br />';
    $output[] = selectAccountLocal('acc_id', $acc_id).'<br />';
    $output[] = selectAccStatus("acc_status", $acc_status??'any');
    $output[] = 'Min <input name = "min" type ="number" max= "0" value="'.$min.'" /><br />';
    $output[] = 'Max <input name = "max" type ="number" min= "0"  value="'.$max.'" /><br />';
    $output[] = 'Url <input name = "url" /><br />';
    $output[] = 'Warning: overriding Url can break the link with other nodes.<br />';
    $output[] = '<input type = "submit" name = "overridePolicy" value="Override" />';
  }

  static function policyFilter(&$output) {
    global $chars, $acc_status, $fullPolicy, $policyFilter;
    $output[] = '<h3>Filter account names</h3>';
    $output[] = '<p><label>Filter policies by characters, status, and show results in three forms.</label><br />';
    if ($policyFilter) {
      $output[] = getResponse();
    }
    $output[] = '<input name = "chars" type="text" placeholder="a..." value="'.($chars ?? '').'" /><br />';
    $output[] = selectAccStatus("acc_status", $acc_status);
    $output[] = selectFullPolicy("fullPolicy", $fullPolicy);
    $output[] = '<p><input type = "submit" name = "policyFilter" value=Filter />';
  }


  static function doBlogic(&$output) {
    global $doBlogic;
    $output[] = "<h3>Append fees onto a transaction</h3>";
    $output[] = "<p>See blogicConfig method for this node's fee structure.<br />";
    if ($doBlogic) {
      $output[] = getResponse();
    }
    $output[] = '<textarea name="example_transaction" rows = "7" cols = "40">{
  "payer": "alice",
  "payee": "bob",
  "description": "blah blah",
  "quant": "10",
  "type": "bill"
}</textarea>';
    $output[] = "<p>An example transaction object is supplied.<br />";
    $output[] = '<input type = "submit" name = "doBlogic" value="Append" />';
  }

  static function setBLogic(&$output) {
    $output[] = "<h3>Not defined yet</h3>";
  }

  static function getActions(&$output) {
    $output[] = "<h3>Get the actions for the current user on a given transaction</h3>";
    $output[] = "Todo - select & submit a transaction";
  }

  static function getWorkflows(&$output) {
    global $getWorkflows, $workflowEditRequester;
    $output[] = '<p>Workflows must be shared between nodes who would share transactions. Therefore workflows are read from rootwards nodes.</p>';
    if ($getWorkflows) {
      $output[] = getResponse();
    }

    $output[] = '<input type = "submit" name = "getWorkflows" value="View" />';
  }

  static function handshake(&$output) {
    global $handshake;
    $output[] = '<p>Check that connected ledgers are online and the hashes match.</p>';
    if ($handshake) {
      $output[] = getResponse();
      $output[] = "200 means ok, 409 means hash mismatch</br />";
    }
    $output[] = '<input type = "submit" name = "handshake" value="Handshakes" />';
  }

}


function getDivTag($attributes, $content) {
  foreach ($attributes as $at => $vals) {
    $atts[] = $at .'="'.implode(' ', $vals).'"';
  }
  return '<div '.implode(' ', $atts) .'>'.$content.'</div>';
}

function getFormTag($attributes, $content) {
  foreach ($attributes as $at => $vals) {
    $atts[] = $at .'="'.implode(' ', $vals).'"';
  }
  return '<form method=post '.implode(' ', $atts) .'>'.$content.'</form>';
}

function selectAccount(string $element_name, $default_val = '', $class = '') {
  $callback = is_isolated_node() ? 'selectAccountLocal': 'selectAccountTree';
  return $callback($element_name, $default_val, $class);
}

/**
 * Make a select widget with all the account names
 * @param string $element_name
 * @param string $default_val
 * @param string $class
 * @return string
 */
function selectAccountLocal(string $element_name, $default_val = '', $class = '') : string {
  global $policyRequester;
  static $all_account_names = [];
  $output[] = '<select name="'.$element_name.'" title="Don\'t like select? Autocomplete is also possible!" class="'.$class.'">';
  if (empty($all_account_names)) {
    $all_account_names = $policyRequester->filter([], 'nameonly');
    ksort($all_account_names);
  }
  foreach ($all_account_names as $name) {
    $output[] = '  <option value="'.$name.'" '.($name == $default_val ? 'selected' : '').'>'.$name.'</option>';
  }
  $output[] = '</select><br />';
  return implode("\n", $output);
}

function selectAccountTree(string $element_name, $default_val = '', $class = '') : string {
  return '<input name = "'.$element_name.'" type="text" placeholder="ancestors/node/account" value="'. $default_val .'" class="'.$class.'" title="Reference any account in the tree using an absolute or relative address. Note that you are only entitled to insepct rootwards nodes, though others may expose their data." />';
}

function selectAccStatus(string $element_name, $default_val = '') {
  $output[] = "<select name=\"$element_name\">";
  $output[] = '  <option value="active"'.($default_val=='active'?'selected':'').'>Active</option>';
  $output[] = '  <option value="any"'.($default_val=='any'?'selected':'').'>Any</option>';
  $output[] = '  <option value="blocked"'.($default_val=='blocked'?'selected':'').'>Blocked</option>';
  $output[] = '</select>';
  return implode("\n", $output);
}

function selectFullPolicy(string $element_name, $default_val = '') {
  $output[] = "<select name=\"$element_name\">";
  $output[] = '  <option value="full"'.($default_val=='full'?'selected':'').'>Full policy, showing defaults</option>';
  $output[] = '  <option value="override"'.($default_val=='override'?'selected':'').'>Saved values without defaults</option>';
  $output[] = '  <option value="nameonly"'.($default_val=='nameonly'?'selected':'').'>Name only</option>';
  $output[] = '</select>';
  return implode("\n", $output);
}

function setResponse($value, $col = 'green') {
  global $response;
  $response = "<font color=$col>".print_r($value, 1)."</font>";
}

/**
 *
 * @global string $response
 * @return string
 */
function getResponse() {
  global $response;
  if ($response) {
    $response = '<pre>'.$response.'</pre>';
  }
  return $response;
}

/**
 * Determine if the node is isolated by asking the policy service
 */
function is_isolated_node() : bool {
  global $policyRequester;
  static $result;
  if (!isset($result)) {
    $result = TRUE;
    // We don't have a way to filter by url <> 0
    foreach ($policyRequester->filter([], 'full') as $policy) {
      if (!empty($policy->url)) {
        $result = FALSE;
        break;
      }
    }
  }
  return $result;
}

function get_devel_log() : string {
  global $log_file;
  $output = '';
  if (file_exists($log_file)) {
    $log = file_get_contents($log_file);
    if (strlen($log) > 10) {
      $output= '<div class="log" title="put cc_log($expression); anywhere in the code"><h3>Debug messages</h3>'.$log.'</div>';
    }
  }
  @unlink('../devel.log');
  return $output;
}

function showInfo() : string {
  global $info;
  $output = '';
  if ($info) {
    $output = '<div class ="feedback" title="The request url and body, and any errors returned.">';
    $output .= implode("\n<br />", $info);
    $output .= '</div>';
  }
  return $output;
}

function get_api_log(array $services) : string {
  include '../node_template/ledgerService/Entry.php';
  include '../node_template/ledgerService/Transaction.php';
  include '../node_template/ledgerService/Transversal/TransversalTransaction.php';
  include '../node_template/ledgerService/AccountStatus.php';
  foreach (array_keys($services) as $activeService) {
    if ($contents = @file_get_contents('../'.$activeService.'.api.log')) {
      $link = '<a href="https://gitlab.com/credit-commons-software-stack/credit-commons-microservices/-/raw/master/docs/openapi-3.0/'.$activeService.'.yml" title="See the API formally described in OpenAPI format" target="_blank"> View OpenAPI</a>';
      $link .= ' <a class="collapsible" title="API calls and latest responses for '.$activeService.' service">View '.$activeService .'API Log</a>';
      $link .= '<div class="collapsible-content" title="API calls and latest responses for '.$activeService.' service"><pre>'.print_r(unserialize($contents), 1).'</pre></div>';
      $output[] = $link;
    }
  }
  return implode('</br />', $output);
}

function credcom_config_save($service) {
  global $active_node;
  extract($_POST);
  $values = $active_node->getRequester($service)->getConfig();
  $setVals = [];
  foreach ($values as $key => $val) {
    if (isset($$key)) {
      $setVals[$key] = $$key;
    }
  }
  cc_log($setVals);
  $active_node->getRequester($service)->setConfig($setVals);
}

function topTransactions() {
  global $ledgerRequester;
  $transactions = [];
  // Show this user's actions at the top of the page.
  if (isset($_POST['createTransaction'])) {
      $main = (object)[
      'payer' => trim($_POST['payer']),
      'payee' => trim($_POST['payee']),
      'quant' => trim($_POST['quant']),
      'description' => trim($_POST['description']),
      'type' => $_POST['type']
    ];
    if ($transaction = $ledgerRequester->print()->buildNewTransaction($main)) {
      setResponse($transaction);
      $transactions[] = $transaction;
    }
  }
  // for the current user to sign
  if ($pending = $ledgerRequester->filter(['state'=> 'pending', 'full' => 1])) {
    foreach ($pending as $t) {
      if (isset($t->actions->completed)) {
        $transactions[] = $t;
      }
    }
  }
  if ($transactions) {
    print '<div class="attention" title="This box is just to show transactions requiring the current user\'s attention">Pending transactions for '.$_GET['acc'].' to sign:';
    print makeTransactionSentences($transactions) .'</div>';
  }
}


function loginOptions(array $accs) {
  global $active_node;
  ksort($accs);
  $admin = $active_node->getRootConfig('admin');
  if ($accs) {
    if (isset($_GET['client'])) {
      print '<!-- choose from accounts on this ledger --> <div title = "Connect using one of the accounts on this ledger">';
      print '<span title="Only users involved in transactions (and admins) can see them before they are confirmed.">Connect to '.$_GET['client'].' as</span>';
      foreach ($accs as $acc) {
        $checked = @$_GET['acc'] == $acc ? 'checked' : '';
        print "\n".'<input type="radio" name="acc" value="'.$acc.'" '.$checked .' onclick="window.location=\'index.php?client='.$active_node->name.'&acc='.$acc.'\'"/>'.$acc .($acc == $admin?' <span title="Admin can do more things to more transactions">(admin)</span>':'');
      }
      print '</div>';
    }
  }
  elseif ($active_node->getRootConfig('dev_mode')) {
    print '<span title="This node has no local accounts and you can only connect as a foreign account with the hashchain info, so for now this node allows you to connect as admin">Connected to '.$_GET['client'].' as admin.</span>';
    $_GET['acc'] = '-admin-';
  }
  else {
    print '<span title="This node has no local accounts and does not allow you to connect as admin">Sorry no authorisation possible.</span>';
  }
}