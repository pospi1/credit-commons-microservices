<?php
ini_set('display_errors', 1);
require_once "../node_template/common.php";
require_once '../node_template/ledgerService/db.php';

global $nodes;
$nodes = [];
$dir = new DirectoryIterator('../');
foreach ($dir as $fileinfo) {
  if ($fileinfo->isFile()) {
    continue;
  }
  if ($fileinfo->getFilename() == 'node_template') {
    continue;
  }
  $nodeini = $fileinfo->getPathName() .'/node.ini';
  if (file_exists($nodeini)) {
    $vars = parse_ini_file($nodeini);
    $nodes[$fileinfo->getFilename()] = new Node($fileinfo->getFilename(), $vars['node_url']);
  }
}
// Alphabetical order?
ksort($nodes);
@unlink('../devel.log');

/**
 * @param string $node_url
 *   The node $node_url without the http://
 * @param array $accounts
 * @param array $options
 *   contains $parent $payer_fee $payee_fee $db_user $db_pass and $mode
 */
class Node {

  public $name;
  public $url;

  /**
   * @param string $node_name
   * @param string $node_url
   */
  function __construct(string $node_name, string $node_url) {
    $this->name = $node_name;
    $this->url = $node_url;
  }

  /**
   *
   * @param string $mode
   * @param string $user
   * @param string $pass
   * @return bool
   *   TRUE on success.
   */
  function generate(string $mode, string $user = 'root', string $pass = '') : bool {
    global $nodes;
    // Copy (or make links to) all the files.
    if (!$this->copyFromTemplate($mode)) {
      clientAddError("unable to replicate directory at ".$node_dir);
      return FALSE;
    }
    $this->setRootConfig('node_url', $this->url);
    //Because the vHosts and hosts file aren't set up yet, these settings must
    //be changed directly, not using the Requester and the API
    $ledgerInifile = "../$this->name/ledgerService/ledger.ini";
    $lines = file($ledgerInifile, FILE_IGNORE_NEW_LINES);
    setConfig($lines, 'db_user', $user);
    setConfig($lines, 'db_pass', $pass);
    setConfig($lines, 'db_name', $this->getDbName());
    file_put_contents($ledgerInifile, implode("\n", $lines));
    $this->makeDb($this->getDbName());
    $nodes[$this->name] = $this;
    return TRUE;
  }

  /**
   *
   * @param array $accounts
   * @param string $payee_fee
   * @param string $payer_fee
   * @param string $parent_url
   * @param type $rate
   */
  function init(array $accounts, $payee_fee, $payer_fee, $parent_url, $rate = 1) {
    if (!empty($payer_fee) or !empty($payee_fee)) {
      $this->addFees('fees', $payee_fee, $payer_fee);
    }
    foreach (array_filter($accounts) as $name) {
      $this->addAccount(trim($name));
    }
    if ($parent_url) {
      if (!is_numeric($rate) and count($div = explode('/', $rate)) == 2) {
        $rate = $div[0]/$div[1];
      }
      elseif (!is_numeric($rate)) {
        echo "Rate '$rate' must be numeric or expressed as a fraction e.g. 2/7";
        exit;
      }
      $this->addParentAccount($parent_url, $rate);
    }
    else {
      clientAddInfo("$this->name has no parent");
    }
  }

  /**
   * Create a database for the new node
   * @param string $node_name
   * @param string $node_url
   * @param array $settings
   * @return string
   */
  function makeDb($db_name) : string {
    $user = $this->get('ledger', 'db_user');
    $pass = $this->get('ledger', 'db_pass');
    $connection = new mysqli('localhost', $user, $pass);
    $connection->query("DROP DATABASE $db_name");
    $connection->query("CREATE DATABASE $db_name");
    Db::connect($db_name, $user, $pass);
    foreach (explode(';', file_get_contents('../node_template/ledgerService/db.sql')) as $q) {
      if ($query = trim($q)) {
        Db::query($query);
      }
    }
    clientAddInfo("Database $db_name created.");
    return $db_name;
  }

  /**
   * @param string $node_url
   * @return string
   */
  function getDbName() : string {
    $name = str_replace(['.', '-', ' ', '/'], '', $this->name);
    return 'credcom_'.strtoLower($name);
  }

  /**
   * Write values to a node service ini file.
   * @param array $values
   * @param string $service
   * @return boolean
   *   TRUE on success
   */
  private function set(array $values, $service) {
    $result = $this->getRequester($service)->setConfig($values);
    if (!$result) {
      foreach($values as $key => $val) {
        clientAddInfo("Set $key to '$val'");
      }
      return TRUE;
    }
    else {
      foreach($result as $message) {
        clientAddInfo($message);
      }
    }
    return FALSE;
  }

  // There isn't an api for editing node.ini because its not in a service.
  // So this is only possible on local nodes.
  private function setRootConfig(string $name, $value) {
    $nodeini = "../$this->name/node.ini";
    $lines = file($nodeini);
    setConfig($lines, $name, $value);
    file_put_contents($nodeini, implode("\n", $lines));
  }

  function getRootConfig(string $var_name) {
    $vars = parse_ini_file("../$this->name/node.ini");
    return $vars[$var_name];
  }

  /**
   * @param string $service
   * @param string $name
   * @return string
   */
  function get(string $service, string $name) : string {
    $vars = parse_ini_file($this->inifilename($service));
    return $vars[$name];
  }

  /**
   *
   * @param string $service
   * @return string
   *   The local filename on the server.
   */
  private function inifilename(string $service) :string {
    if ($service) {
      return "../$this->name/{$service}Service/{$service}.ini";
    }
    else {
      return "../$this->name/node.ini";
    }
  }

  /**
   * Get the virtualHost name of this node.
   * @return string
   */
  public function getHostname() {
    return parse_url($this->url)['host'];
  }

  /**
   * @param string $dir
   *   The name of the directory
   * @param string $mode
   *   copy or link, name of the php function
   * @return bool
   *   TRUE on success
   */
  function copyFromTemplate($mode = 'copy') :bool {
    chdir('../');
    if (is_dir($this->name)) {
      if (!self::deleteDir()) {
        clientAddError("wasn't able to delete $this->name directory in ".getcwd());
        return FALSE;
      }
    }
    if (!mkdir($this->name)) {
      clientAddError("wasn't able to create $this->name directory in ".getcwd());
      return FALSE;
    }
    $linkable_extensions = ['php', 'htaccess', 'htm', 'sql'];
    $files = new RecursiveDirectoryIterator('node_template', FilesystemIterator::SKIP_DOTS);
    $iterator = new \RecursiveIteratorIterator($files);
    foreach ($iterator as $fileinfo) {
      $new = str_replace('node_template', $this->name, $fileinfo->getRealPath());
      if (!is_dir(dirname($new))){
        mkdir(dirname($new));
      }
      $op = in_array($fileinfo->getExtension(), $linkable_extensions) ? $mode : 'copy';
      $op($fileinfo->getPathname(), $new);
    }
    chdir('ccclient');
    return TRUE;
  }

  /**
   * Check the url is a credit commons node, and then add it as a parent account.
   *
   * @param string $url
   */
  function addParentAccount($url, $rate = 1, $local = FALSE) : void {
    global $nodes;
    if ($parent_node_name = $this->checkParent($url)) {
      $ini = ['bot_account' => $parent_node_name, 'bot_rate' => $rate];
      $this->set($ini, 'ledger');
      $this->addAccount($parent_node_name, $url);
      $nodes[$parent_node_name]->addAccount($this->name, $this->url);
      //now ping it.
      list ($code, $details) = $this->getRequester('ledger')->handshake();
      if ($code == 200) {
        clientAddInfo("Successfully connected to the parent node with rate ".$rate);
      }
      else {
        clientAddInfo("There was a $code problem connecting to the parent node");
      }
    }
  }

  public function ping() {
    return $this->getRequester('ledger')->handshake();
  }

  /**
   * Test the new (remote) url and retrive the node name
   * @param string $url
   * @return string
   *   The name of the parent node.
   */
  private function checkParent(string $url) {
    // Peer certificate CN=`cavesoft.net' did not match expected CN=`ledger.demo.credcom.dev
    stream_context_set_default([
      'ssl' => [
        'peer_name' => 'generic-server',
        'verify_peer' => FALSE,
        'verify_peer_name' => FALSE,
        'allow_self_signed'=> TRUE
      ]
    ]);
    // We can't use the requester here because the client requester only requests
    // from the current node. Also because this request is to the top level of
    // the node, not to a service.
    if (@file_get_contents($url)) {
      foreach ($http_response_header as $header) {
        if (preg_match('/^Node-name: ?(.*)$/', $header, $matches)) {
          clientAddInfo('Parent node '.$url. ' is online');
          return $matches[1];
        }
      }
    }
    clientAddError('Could not ping parent node '.$url);
  }

  /**
   *
   * @param string $name
   *   The desired name of the fees account
   * @param string $payee_fee
   *   a fixed number or percentage of the transaction to charge
   * @param string $payer_fee
   *   a fixed number or percentage of the transaction to charge
   */
  function addFees(string $name, string $payee_fee, string $payer_fee) {
    $this->addAccount($name);
    $ini = ['fees_account' => $name];
    // blogic.ini state the fees and fees acount
    if ($payee_fee) {
      $ini['payee_fee'] = $payee_fee;
    }
    if ($payer_fee) {
      $ini['payer_fee'] = $payer_fee;
    }
    $this->set($ini, 'blogic');
  }

  /**
   * Add an account to a node
   * @param string $name
   * @param string $url
   * @todo This doesn't use the API
   */
  function addAccount($name, $url='') {
    $this->getRequester('policy')->join($name);
    if ($url) {
      $this->getRequester('policy')->override($name, ['url' => $url]);
    }
    clientAddInfo("Created account $name on $this->name");
  }

  /**
   * Empty the ledger database.
   */
  function truncate() {
    Db::connect(
      $this->get('ledger', 'db_name'),
      $this->get('ledger', 'db_user'),
      $this->get('ledger', 'db_pass')
    );
    Db::query('TRUNCATE TABLE temp');
    Db::query('TRUNCATE TABLE transactions');
    Db::query('TRUNCATE TABLE entries');
    Db::query('TRUNCATE TABLE log');
  }

  /**
  *
  * @param string $activeService
  * @return Requester
  */
  function getRequester(string $activeService) : BaseRequester {
    $activeService = ucFirst($activeService).'Requester';
    // The client requesters usually depend on the main requesters.
    require_once "../node_template/ledgerService/requesters/BaseRequester.php";
    @include_once "../node_template/ledgerService/requesters/{$activeService}.php";
    require_once 'requesters/ClientRequesterTrait.php';
    $requester_class = "Client{$activeService}";
    require_once 'requesters/'.$requester_class.'.php';
    // Can't use the create method and its caching coz we don't have the Remote account object here.
    return new $requester_class($this->url);
  }

  /**
   * Get the html and javascript of the balance chart
   * @return array
   */
  function renderChart() : array {
    $map = $this->getRequester('ledger')->accounts(TRUE, FALSE);
    $id = "balance_map_$this->name";
    return [
      '<div id = "'.$id.'" style="width:24%"></div>',
      $this->getChart($id, $map)
    ];
  }

  /**
   * Get the javascript chart
   * @param string $id
   * @param array $accounts
   * @return string
   */
  function getChart(string $id, array $accounts) : string {
    $output = '
    var data = google.visualization.arrayToDataTable([
      ["Acc", "Balance", { role: "style" }],
      ["", 0, ""]
    ]);
    var options = {
      title: "Account balances on '.$this->name.'",
      height: "'.(count($accounts)*16 + 65).'",
      bar: {groupWidth: "95%"},
      legend: "none"
    };
    var chart = new google.visualization.BarChart(document.getElementById("'.$id.'"));
    chart.draw(data, options);
  ';
    $data = [];
    $policies = $this->getRequester('policy')->filter();
    foreach ($accounts as $acc_name => $info) {
      $color = empty($policies[$acc_name]->url) ? 'black' : 'gray';
      $data[] = "['$acc_name', $info->balance, '$color']";
    }
    if ($data) {
      $output = str_replace('["", 0, ""]', implode(",\n    ", $data), $output);
    }
    return $output;
  }

  /**
  * Thanks to https://stackoverflow.com/questions/3349753/delete-directory-with-files-in-it
  * @return bool
  *   TRUE on success
  */
 function deleteDir($dirPath = '') : bool {
   if (!$dirPath) {
     $dirPath = $this->name;
   }
   if (!is_dir($dirPath)) {
     throw new InvalidArgumentException("$dirPath must be a directory");
   }
   if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
     $dirPath .= '/';
   }
   $files = new RecursiveDirectoryIterator($dirPath, FilesystemIterator::SKIP_DOTS);
   foreach ($files as $file) {
     if ($file->isDir()) {
       if (!self::deleteDir($file->getPathname())) {
         clientAddError('unable to delete directory '.$file);
         return FALSE;
       }
     }
     else {
       if (!unlink($file->getPathname())) {
         clientAddError('unable to delete file '.$file);
         return FALSE;
       }
     }
   }
   return rmdir($dirPath);
 }
}

/**
 *
 * @global string $info
 * @param mixed $message
 */
function clientAddInfo($message) {
  global $info;
  if ($message) {
    if (!is_string($message)) {
      $message = '<pre>'.print_r($message, 1).'</pre>';
    }
    $message = str_replace('BoT', '<span title="Balance of Trade account">BoT</span>', $message);
    $info[] = '<font color="green">'.$message.'</font>';
  }
}

/**
 *
 * @global string $info
 * @param mixed $message
 */
function clientAddError($message) {
  global $info;
  if (!is_string($message)) {
    $message = '<pre>'.print_r($message, 1).'</pre>';
  }
  $message = str_replace('BoT', '<span title="Balance of Trade account">BoT</span>', $message);
  $info[] = '<font color="red">'.$message.'</font>';
}

function balance_charts_template($js_charts) : string {
  return chart_library().'
    <script type="text/javascript">
      function drawBalanceCharts() {
        '.$js_charts.'
      }
      google.charts.setOnLoadCallback(drawBalanceCharts);
    </script>
  ';
}

function chart_library() {
  static $done = FALSE;
  if (!$done) {
    return '<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>'
    . '<script type="text/javascript">google.charts.load("current", {packages:["corechart"]});</script>';
  }
  $done = TRUE;
}

function node_tree() {
  global $nodes;
  // recursively build the node tree
  $tree = [];
  foreach ($nodes as $name => $node){
    $parents[$name] = $node->get('ledger', 'bot_account');
  }
  asort($parents);
  local_node_branches($parents, $tree);
  return $tree;
}

/**
 *
 * @global array $nodes
 * @return array
 *   the html divs and the javascript
 */
function print_all_charts() : void {
  global $nodes;
  $tree = node_tree();

  $levels = $charts = [];
  $level = 1;
  $output = '<p>The following charts show the account balances on different ledgers.
    Each ledger is in principle, independent technologically, politically and monetarily.
    The left and right sides of each ledger are balanced, because credit = debt.</p>';
  $iterator = new RecursiveIteratorIterator(
    new RecursiveArrayIterator($tree), RecursiveIteratorIterator::SELF_FIRST
  );
  foreach ($iterator as $node_name => $array) {
    $level = $iterator->getDepth();
    list ($div, $js_chart) = $nodes[$node_name]->renderChart();
    $charts[] = $js_chart;
    $levels[$level][] = $div;
  }
  print balance_charts_template(implode("\n", $charts));

  foreach (array_reverse($levels) as $l => $renders) {
    $output .=  "\n<div class=\"balance-charts level-$l\" align=\"center\">";
    $output .= implode("\n", $renders);
    $output .= "\n</div>";
  }
  $output .="<p>The upper ledgers have accounts for ordinary users, while the lower ledgers have accounts for ledgers on the level above.
    Just as a ledger represents an agreement to exchange between individual users, the lower ledgers represent agreements to exchange between groups of users.
    The grey lines represent the balances which are cryptographically tied to an adjacent ledger.
    The names of the grey accounts correspond but the numbers do not because each ledger has its own unit of value.
  </p>";
  print $output;
}

function get_one_balance_chart(Node $node) : string {
  list ($div, $js_charts) = $node->renderChart();
  //return '';
  return $div . balance_charts_template($js_charts);
}

function get_one_history_chart(string $name, array $data) : string {
  $id = "history_chart_$name";
  return chart_library() .
    populate_history_chart_template($name, $id, $data) .
    '<div class = "history-chart" id = "'.$id.'"></div>';
}

function populate_history_chart_template(string $name, string $id, array $data) : string {
  $js = '<script>
google.charts.setOnLoadCallback($id);
function $id() {
  var data = new google.visualization.arrayToDataTable([
    ["Date", "value"],
    $rows
  ]);
  var options = {
    title: "History of $name",
    width: 300,
    height: 200,
    legend: {position: "none"}
  }
  new google.visualization.LineChart(document.getElementById("$id")).draw(data, options);
}</script>';
  $js = str_replace('$name', $name, $js);
  $js = str_replace('$id', $id, $js);
  foreach ($data as $date => $balance) {
    $rows[] = "[new Date('$date'), $balance]";
  }
  return str_replace('$rows', implode(",\n    ", $rows), $js);

}

/**
 * Recursive function to build local tree.
 * @param type $parents
 * @param array $empty_branch
 * @param type $branchname
 */
function local_node_branches($parents, &$empty_branch, $branchname = '') {
  static $depth = 1;
  foreach ($parents as $node_name => $parent_name) {
    if ($parent_name == $branchname) {
      $empty_branch[$node_name] = [];
      local_node_branches($parents, $empty_branch[$node_name], $node_name);
    }
  }
  $depth++;
}

/**
 * Object representing the server to help make config.
 */
abstract class ServerConfigurer {

  const SERVICES = ['blogic', 'ledger', 'policy', 'workflowEdit'];

  /**
   * Instantiate an object of the right class according the server software.
   * @return \class
   */
  static function create() : ServerConfigurer {
    $class = 'Server'.self::software();
    return new $class();
  }

  /**
   * Write the server file in the top level of this repository
   *
   * @return int
   *   the number of chars written
   */
  function setup() {
    global $nodes;
    $root = $this->nodeRoot();
    $conf = '#autogenerated by ServerConfigurer::setup in '.__FILE__."\n";
    $conf .= str_replace(
      ['__ROOT__', '__SERVERNAME__'],
      [$root, $_SERVER['SERVER_NAME']],
      static::CLIENT_TEMPLATE
    );
    foreach ($nodes as $node) {
      $conf .= $this->generateServerConf($node). "\n\n";
    }
    $this->write($conf);
    return $conf;
  }

  /**
   * Get the apache conf directives for the node.
   * @param Node $node
   * @return string
   *   The conf for that node.
   */
  private function generateServerConf(Node $node) : string {
    $host_name = $node->getHostname();
    $root = $this->nodeRoot() .'/'.$node->name;
    $vhosts = [];

    $vhosts[] = str_replace(
      ['__ROOT__', '__DIR__', '__SERVERNAME__', '__NODENAME__'],
      [$root, '', $host_name, $node->name],
      static::VHOST_TEMPLATE
    );
    $vhosts[] = '';
    foreach (static::SERVICES as $service) {
      $service_host =  strtolower($service).'.'.$host_name;
      $service_name = $service.'Service';
      $vhosts[] = str_replace(
        ['__SERVERNAME__', '__ROOT__', '__DIR__', '__NODENAME__'],
        [$service_host, $root, $service_name, $node->name],
        static::VHOST_TEMPLATE
      );
    }
    return implode("\n\n", $vhosts);
  }
  /**
   *
   * @param type $conf
   * @return string
   *   The conf for that node.
   */
  function write($conf) {
    $file = $this->getConfFile();
    if (file_put_contents($this->getConfFile(), $conf)) {
      chmod($file, 0444);
      clientAddInfo("New virtualHosts have been added to $file. <strong>You MUST restart ".self::software()." before continuing.</strong>");
    }
    else {
      clientAddError("Failed to write to $file. Check file permissions and resubmit the form.");
    }
  }

  /**
   * Determine whether the server is Apache or Nginx
   * @return string
   */
  static function Software() :string {
    return ucfirst(substr($_SERVER['SERVER_SOFTWARE'], 0, strpos($_SERVER['SERVER_SOFTWARE'], '/')));
  }

  final function getConfFile() {
    return $this->NodeRoot().'/'.static::CONF_FILE;
  }

  final function nodeRoot() {
    return dirname(__DIR__);
  }

  /**
   * Get the hosts file entries for the node
   * @param Node name
   * @return string
   *   All the services on one line.
   */
  final function getNodeHosts(Node $node) {
    $hostname = $node->getHostname();
    $hosts = [$hostname];
    foreach (static::SERVICES as $service) {
      $hosts[] = strtolower($service).'.'.$hostname;
    }
    return "127.0.0.1\t".implode(' ', $hosts);
  }

  /**
   * Show lines needed in the hosts file.
   */
  function showHosts() {
    global $nodes;
    foreach ($nodes as $node_name => $node) {
      $hosts[$node_name] = $this->getNodeHosts($node);
    }
    clientAddInfo("Ensure your hosts file has the following lines:\n<pre>".implode("\n", $hosts)."</pre>");
  }

}

/**
 * Class to help build server config
 */
class ServerApache extends ServerConfigurer {
  const CONF_FILE = 'apache.conf';
  const CLIENT_TEMPLATE = '<Directory __ROOT__>
  AllowOverride All
  Require all granted
</Directory>
';

  const VHOST_TEMPLATE = "<VirtualHost *:80>
  DocumentRoot __ROOT__/__DIR__
  ServerName __SERVERNAME__
  php_value session.cookie_domain .__NODENAME__
  ErrorLog __ROOT__/cc_error.log
</VirtualHost>";
}

class ServerNginx extends ServerConfigurer{
  const CONF_FILE = 'nginx.conf';
  const CLIENT_TEMPLATE = '';
  const VHOST_TEMPLATE = '
server {
  listen 80;
  server_name __SERVERNAME__;
  root __ROOT__/__DIR__/;
  error_log __ROOT__/cc_error.log;
  location ~ \.php(?:$|/) {
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    # fastcgi_params is a default conf file in /etc/nginx
    include fastcgi_params;
    fastcgi_param  SCRIPT_NAME \'index.php\';
    #todo this needs to be configurable.
    fastcgi_pass php-handler;
    fastcgi_param PHP_VALUE session.cookie_domain=.__NODENAME__;
  }
}';

}

function mkurl($node_name) {
  return $_SERVER['REQUEST_SCHEME'].'://'.$node_name .'.'.$_SERVER['SERVER_NAME'] ;
}
