<?php


/**
 * Handle requests & responses from the client to the ledger.
 */
class ClientPolicyRequester extends PolicyRequester {
  use ClientRequesterTrait;

  function __construct($other_domain) {
    $this->serviceUrl = str_replace('://', '://policy.', $other_domain);
  }

  /**
   * @param string $name
   * @return stdClass|null
   *   An object representing the group's policy towards the new account
   */
  function join($name) {
    list($code, $accountPolicy) = $this->setMethod('post')
      ->accept(201, 400)
      ->addField('name', $name)
      ->request('join');

    switch ($code) {
      case 201:
        return $accountPolicy;
      case 400:
        clientAddError('An account with that name already exists.');
        break;
    }
  }

}
