<?php


/**
 * Handle requests & responses from the client to the ledger.
 */
class ClientWorkflowEditRequester extends BaseRequester {
  use ClientRequesterTrait;

  const SUBDOMAIN = 'workflow';

  function __construct(string $other_domain) {
    $this->serviceUrl = str_replace('://', '://workflowedit.', $other_domain);
  }


  /**
   * List all the available workflows, keyed by md5 hash
   */
  function list() {
    list($code, $wfs) = $this->accept(200)->request('list');
    return $wfs;
  }


  function view($name_or_hash) {
    list($code, $workflow) = $this->accept(200)->request($name_or_hash);
    return $workflow;
  }

  // get the workflow name from the hash
  function getName($hash) {
    list($code, $name) = $this->accept(200, 403)->request('name/'.$hash);
    if ($code == 403) {
      cc_violation(WorkflowMismatchViolation::Create($hash));
    }
    return $name;
  }

  /**
   * Get a list of actions the current user can perform on the transaction.
   * @param Transaction $transaction
   * @return array
   */
  function getActions(Transaction $transaction) {
    list($code, $actions) = $this
      ->accept(200)
      ->setMethod('post')
      ->setBody($transaction)
      ->request('actions');
    if ($code == 200) {
      return (array)$actions;
    }
    else {
      cc_log('Could not load actions: '.$code);
    }

  }

  function sign(Transaction $transaction, $target_state) {
    list($code, $signatures) = $this
      ->accept(200, 201, 403)
      ->setMethod('PATCH')
      ->setBody($transaction)
      ->request('sign/'.$target_state);

    switch ($code) {
      case 200:
        return [$transaction->state, $signatures];
      case 201:
        return [$target_state, []];
      case 403:
        cc_violation(WorkflowViolation::create($transaction->uuid, $target_state, $_SESSION['user']));
    }
  }

}
